<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login','UserController@login');
Route::post('/user/create','UserController@signUp');
Route::get('/department/get','Department\DepartmentController@allDepartment');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware(['auth:api'])->group(function () {
    Route::get('Department/Material/get','Department\MaterialController@allMaterialDepartment');
    Route::post('Material/create','Department\MaterialController@createMaterial');
    Route::post('Material/update','Department\MaterialController@updateMaterial');
    Route::post('Material/delete','Department\MaterialController@deleteMaterial');
    Route::post('Department/create','Department\DepartmentController@createDepartment');
    Route::post('Department/update','Department\DepartmentController@updateDepartment');
    Route::post('Department/delete','Department\DepartmentController@deleteDepartment');
    Route::post('Question/create','Exam\QuestionController@createQuestion');
    Route::post('Question/update','Exam\QuestionController@updateQuestion');
    Route::post('Question/delete','Exam\QuestionController@deleteQuestion');
    Route::get('question/material','Exam\QuestionController@getQuestionMaterial');
    Route::post('Exam/create','Exam\ExamController@createExam');
    Route::post('Exam/submit','Exam\ExamController@submit');
    Route::get('Exam/get','Exam\ExamController@getExam');
    Route::get('Exam/user/get','Exam\ExamController@getUserExam');
    Route::post('Proposal/create','Feedback\ProposalQuestionController@createProposal');
    Route::post('Proposal/delete','Feedback\ProposalQuestionController@deleteProposal');
    Route::get('Proposal/material','Feedback\ProposalQuestionController@getProposalMaterial');
    Route::post('Report/create','Feedback\ReportController@createReport');
    Route::post('Report/update','Feedback\ReportController@updateReport');
    Route::post('Report/delete','Feedback\ReportController@deleteReport');
    Route::get('Report/all','Feedback\ReportController@allReport');
    Route::post('feedback/create','Feedback\FeedbackController@createFeedback');
    Route::post('feedback/delete','Feedback\FeedbackController@deleteFeedback');
    Route::get('feedback/all','Feedback\FeedbackController@allFeedback');
});
