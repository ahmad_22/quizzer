<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const table='users';
    const id='id';
    const firstName='first_name';
    const lastName='last_name';
    const email='email';
    const password='password';
    const username='username';
    const type='type';
    const departmentId='department_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable = [self::firstName,self::lastName,self::email,self::password
        ,self::username,self::type,self::departmentId];

    protected $hidden = ['password', 'remember_token', self::updatedAt,self::deletedAt];

    protected $casts = [
        'email_verified_at' => 'datetime',self::departmentId=>'integer'
    ];

    public function createUser($data)
    {
        return self::create($data);
    }

    public function updateUser($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteUser($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteUser($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allUser()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }

    public function login($credentials)
    {
        $user=null;
        if(Auth::attempt($credentials))
        {
            $user=Auth::user();
            $user->token_api=$user->createToken('motorcycle')->accessToken;
        }
        return $user;
    }

    public function logout()
    {
        Auth::user()->oauthAccessTokens()->delete();
    }

}
