<?php

namespace App\Http\Requests\User;

use App\classes\ResponseHelper;
use App\Model\Department\Department;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class signUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            User::username=>['required','string',Rule::unique(User::table,User::username)->whereNull(User::deletedAt)],
            User::password=>['required','string'],
            User::firstName=>['required','string'],
            User::lastName=>['required','string'],
            User::email=>['required','string'],
            User::departmentId=>['required',Rule::exists(Department::table,Department::id)
                ->whereNull(Department::deletedAt)],
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
