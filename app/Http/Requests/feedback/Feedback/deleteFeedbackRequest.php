<?php

namespace App\Http\Requests\feedback\Feedback;

use App\classes\ResponseHelper;
use App\Model\Feedback\Feedback;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class deleteFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Feedback::feedbackId=>['required','integer',Rule::exists(Feedback::table,Feedback::id)
                ->whereNull(Feedback::deletedAt)],
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter($validator->getMessageBag())
        );
    }
}
