<?php

namespace App\Http\Requests\feedback\Report;

use App\classes\ResponseHelper;
use App\Model\Exam\Question;
use App\Model\Feedback\Report;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class createReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Report::questionId=>['required','integer',Rule::exists(Question::table,Question::id)
                ->whereNull(Question::deletedAt)],
            Report::reportText=>['required','string']
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter($validator->getMessageBag())
        );
    }
}
