<?php

namespace App\Http\Requests\feedback\Report;

use App\classes\ResponseHelper;
use App\Model\Feedback\Report;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class updateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Report::reportId=>['required','integer',Rule::exists(Report::table,Report::id)
                ->whereNull(Report::deletedAt)],
            Report::isSee=>['required','boolean']
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter($validator->getMessageBag())
        );
    }
}
