<?php

namespace App\Http\Requests\feedback\proposal;

use App\classes\ResponseHelper;
use App\Model\Feedback\ProposalQuestion;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class deleteProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProposalQuestion::proposalQuestionId=>['required','integer',Rule::exists(ProposalQuestion::table,ProposalQuestion::id)
                ->whereNull(ProposalQuestion::deletedAt)],
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
