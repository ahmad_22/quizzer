<?php

namespace App\Http\Requests\feedback\proposal;

use App\classes\ResponseHelper;
use App\Model\Department\Material;
use App\Model\Feedback\ProposalQuestion;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class createProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProposalQuestion::materialId=>['required','integer',Rule::exists(Material::table,Material::id)
                ->whereNull(Material::deletedAt)],
            ProposalQuestion::question=>['required','string'],
            ProposalQuestion::answer1=>['required','string'],
            ProposalQuestion::isTrue1=>['required','boolean'],
            ProposalQuestion::answer2=>['required','string'],
            ProposalQuestion::isTrue2=>['required','boolean'],
            ProposalQuestion::answer3=>['required','string'],
            ProposalQuestion::isTrue3=>['required','boolean'],
            ProposalQuestion::answer4=>['required','string'],
            ProposalQuestion::isTrue4=>['required','boolean'],

        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
