<?php

namespace App\Http\Requests\Department\Material;

use App\classes\ResponseHelper;
use App\Model\Department\Material;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class deteleMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Material::materialId=>['required','integer',Rule::exists(Material::table,Material::id)
                ->whereNull(Material::deletedAt)],
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
