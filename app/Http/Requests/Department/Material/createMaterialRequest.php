<?php

namespace App\Http\Requests\Department\Material;

use App\classes\ResponseHelper;
use App\Model\Department\Department;
use App\Model\Department\Material;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class createMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Material::departmentId=>['required','integer',Rule::exists(Department::table,Department::id)
                ->whereNull(Department::deletedAt)],
            Material::name=>['required','string']
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
