<?php

namespace App\Http\Requests\Exam\Question;

use App\classes\ResponseHelper;
use App\Model\Exam\Answer;
use App\Model\Exam\Question;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class updateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Question::questionText=>['string'],
            Question::questionId=>['integer',Rule::exists(Question::table,Question::id)
                ->whereNull(Question::deletedAt)],
            'answer'=>['array','min:1'],
            'answer.*.'.Answer::answerId=>['required','integer'],
            'answer.*.'.Answer::answerText=>['required','string'],
            'answer.*.'.Answer::isTrue=>['required','boolean'],
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
