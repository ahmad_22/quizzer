<?php

namespace App\Http\Requests\Exam\Question;

use App\classes\ResponseHelper;
use App\Model\Department\Material;
use App\Model\Exam\Answer;
use App\Model\Exam\Question;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class createQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Question::questionText=>['required','string'],
            Question::materialId=>['required','integer',Rule::exists(Material::table,Material::id)
                ->whereNull(Material::deletedAt)],
            'answer'=>['required','array','min:4','max:4'],
            'answer.*.'.Answer::answerText=>['required','string'],
            'answer.*.'.Answer::isTrue=>['required','boolean'],

        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter()
        );
    }
}
