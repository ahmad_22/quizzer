<?php

namespace App\Http\Requests\Exam\Exam;

use App\classes\ResponseHelper;
use App\Model\Exam\StudentAnswer;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class submitExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer'=>['required','array'],
            'answer.*.'.StudentAnswer::examQuestionId=>['required','integer'],
            'answer.*.'.StudentAnswer::studentAnswerId=>['required','integer'],
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            ResponseHelper::errorMissingParameter($validator->getMessageBag())
        );
    }
}
