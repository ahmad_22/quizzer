<?php

namespace App\Http\Controllers\Exam;

use App\classes\ResponseHelper;
use App\Http\Requests\Exam\Question\createQuestionRequest;
use App\Http\Requests\Exam\Question\deleteQuestionRequest;
use App\Http\Requests\Exam\Question\getQuestionMaterialRequest;
use App\Http\Requests\Exam\Question\updateQuestionRequest;
use App\Model\Exam\Answer;
use App\Model\Exam\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    private $question=null;
    private $answer=null;

    /**
     * QuestionController constructor.
     * @param Question $question
     * @param Answer $answer
     */
    public function __construct(Question$question,Answer $answer)
    {
        $this->question = $question;
        $this->answer = $answer;
    }

    public function createQuestion(createQuestionRequest $request){
        DB::beginTransaction();
        $data=$request->only($this->question->getFillable());
        $data['user_id']=$request->user()->id;
        $created=$this->question->createQuestion($data);
        if(empty($created)) {
            DB::rollBack();
            return ResponseHelper::generalError();
        }
        $inData=$request->get('answer');
        $dataAnswer=array();
        foreach ($inData as $item)
            array_push($dataAnswer,[Answer::questionId=>$created->id,Answer::answerText=>$item[Answer::answerText]
                ,Answer::isTrue=>$item[Answer::isTrue]]);

        $insert=$this->answer->insertAnswer($dataAnswer);
        if(empty($insert)) {
            DB::rollBack();
            return ResponseHelper::generalError();
        }
        DB::commit();
        return ResponseHelper::insert($created);
    }

    public function updateQuestion(updateQuestionRequest $request){
        if(!($request->has(Question::questionId))&&(!$request->has('answer')))
            return ResponseHelper::errorMissingParameter();
        $updated=null;
        if($request->filled(Question::questionId)){
            $id=$request->get(Question::questionId);
            $data=$request->only(Question::questionText);
            $updated=$this->question->updateQuestion([Question::id=>$id],$data);
        }
        if($request->has('answer')){
            $inData=$request->get('answer');
            foreach ($inData as $item) {
                $answerId = $item[Answer::answerId];
                $updated=$this->answer->updateAnswer([Answer::id=>$answerId]
                    ,['answer_text'=>$item[Answer::answerText],'is_true'=>$item[Answer::isTrue]]);
            }

        }
        if(empty($updated))
            return ResponseHelper::generalError();
        return ResponseHelper::update($updated);
    }

    public function deleteQuestion(deleteQuestionRequest $request){
        $id=$request->get(Question::questionId);
        $deleted = $this->question->deleteQuestion([Question::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        return ResponseHelper::delete();
    }
    public function getQuestionMaterial(getQuestionMaterialRequest $request){
        return ResponseHelper::select(
            $this->question->getQuestionMaterial($request->get(Question::materialId)));
    }

}
