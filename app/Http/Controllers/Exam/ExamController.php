<?php

namespace App\Http\Controllers\Exam;

use App\classes\ResponseHelper;
use App\Http\Requests\Exam\Exam\createExamRequest;
use App\Http\Requests\Exam\Exam\getExamRequest;
use App\Http\Requests\Exam\Exam\submitExamRequest;
use App\Model\Exam\Exam;
use App\Model\Exam\ExamQuestion;
use App\Model\Exam\Question;
use App\Model\Exam\StudentAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    private $exam=null;
    private $examQuestion=null;
    private $question=null;
    private $studentAnswer=null;

    /**
     * ExamController constructor.
     * @param Exam $exam
     * @param ExamQuestion $examQuestion
     * @param Question $question
     * @param StudentAnswer $studentAnswer
     */
    public function __construct(Exam $exam,ExamQuestion $examQuestion,Question $question,StudentAnswer $studentAnswer)
    {
        $this->exam = $exam;
        $this->examQuestion = $examQuestion;
        $this->question = $question;
        $this->studentAnswer = $studentAnswer;
    }
    public function createExam(createExamRequest $request){
        DB::beginTransaction();
        $data=$request->only($this->exam->getFillable());
        $data['user_id']=$request->user()->id;
        $created=$this->exam->createExam($data);
        if(empty($created)) {
            DB::rollBack();
            return ResponseHelper::generalError();
        }
        //examQuestion
        $inQuestionIds=$this->question->getRandomQuestion($request->get(Exam::numberQuestion));
        $examQuestion=array();
        foreach ($inQuestionIds as $questionId){
            array_push($examQuestion,[ExamQuestion::examId=>$created->id
                ,ExamQuestion::questionId=>$questionId->id]);
        }
        $insert=$this->examQuestion->insertExamQuestion($examQuestion);
        if(empty($insert)) {
            DB::rollBack();
            return ResponseHelper::generalError();
        }
        DB::commit();
        $questionIds = array_column($examQuestion, 'question_id');
        $examQuestionResult=$this->examQuestion->getExamQuestion($created->id,$questionIds);

        return ResponseHelper::insert($examQuestionResult);
    }

    public function submit(submitExamRequest $request){
        $inAnswers=$request->get('answer');
        $answers=array();
        foreach ($inAnswers as $answer){
            array_push($answers,[StudentAnswer::studentAnswerId=>$answer[StudentAnswer::studentAnswerId]
            ,StudentAnswer::examQuestionId=>$answer[StudentAnswer::examQuestionId]]);
        }
        $insert=$this->studentAnswer->insertStudentAnswer($answers);
        if(empty($insert))
            return ResponseHelper::generalError();
        return ResponseHelper::insert($insert);
    }

    public function getExam(getExamRequest $request){

        return ResponseHelper::select($this->exam->getExam($request->get(Exam::examId)));
    }
    public function getUserExam(Request $request){

        return ResponseHelper::select($this->exam->get([Exam::userId=>$request->user()->id]));
    }

}
