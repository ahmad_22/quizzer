<?php

namespace App\Http\Controllers\Feedback;

use App\classes\ResponseHelper;
use App\Http\Requests\feedback\proposal\createProposalRequest;
use App\Http\Requests\feedback\proposal\deleteProposalRequest;
use App\Http\Requests\feedback\proposal\getProposelMaterialRequest;
use App\Model\Feedback\ProposalQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProposalQuestionController extends Controller
{
    private $proposal=null;

    /**
     * ProposalQuestionController constructor.
     * @param ProposalQuestion $proposal
     */

    public function __construct(ProposalQuestion $proposal)
    {
        $this->proposal = $proposal;
    }

    public function createProposal(createProposalRequest $request){
        $data=$request->only($this->proposal->getFillable());
        $created=$this->proposal->createProposalQuestion($data);
        if(empty($created))
            return ResponseHelper::generalError();

        return ResponseHelper::insert($created);
    }
    public function deleteProposal(deleteProposalRequest $request){
        $id=$request->get(ProposalQuestion::proposalQuestionId);
        $deleted = $this->proposal->forceDeleteProposalQuestion([ProposalQuestion::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        if($deleted==23)
            return ResponseHelper::errorNotAllowed();
        return ResponseHelper::delete();
    }
    public function getProposalMaterial(getProposelMaterialRequest $request){
        return ResponseHelper::select(
            $this->proposal->getProposalMaterial($request->get(ProposalQuestion::materialId)));
    }
}
