<?php

namespace App\Http\Controllers\Feedback;

use App\classes\ResponseHelper;
use App\Http\Requests\feedback\Feedback\createFeedbackRequest;
use App\Http\Requests\feedback\Feedback\deleteFeedbackRequest;
use App\Model\Feedback\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    private $feedback=null;

    /**
     * FeedbackController constructor.
     * @param Feedback $feedback
     */
    public function __construct(Feedback$feedback)
    {
        $this->feedback = $feedback;
    }

    public function createFeedback(createFeedbackRequest $request){
        $data=$request->only($this->feedback->getFillable());
        $data['user_id']=$request->user()->id;
        $created=$this->feedback->createFeedback($data);
        if(empty($created))
            return ResponseHelper::generalError();
        return ResponseHelper::insert($created);
    }

    public function deleteFeedback(deleteFeedbackRequest $request){
        $id=$request->get(Feedback::feedbackId);
        $deleted = $this->feedback->forceDeleteFeedback([Feedback::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        return ResponseHelper::delete();
    }
    public function allFeedback(Request $request){
        return ResponseHelper::select($this->feedback->all());
    }
}
