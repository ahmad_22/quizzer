<?php

namespace App\Http\Controllers\Feedback;

use App\classes\ResponseHelper;
use App\Http\Requests\feedback\Report\createReportRequest;
use App\Http\Requests\feedback\Report\deleteReportRequest;
use App\Http\Requests\feedback\Report\updateReportRequest;
use App\Model\Feedback\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    private $report=null;

    /**
     * ReportController constructor.
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    public function createReport(createReportRequest $request){
        $data=$request->only($this->report->getFillable());
        $data['user_id']=$request->user()->id;
        $created=$this->report->createReport($data);
        if(empty($created))
            return ResponseHelper::generalError();
        return ResponseHelper::insert($created);
    }

    public function updateReport(updateReportRequest $request){
        $id=$request->get(Report::reportId);
        $data=$request->only(Report::isSee);
        $updated=$this->report->updateReport([Report::id=>$id],$data);
        if(empty($updated))
            return ResponseHelper::generalError();
        return ResponseHelper::update($updated);
    }

    public function deleteReport(deleteReportRequest $request){
        $id=$request->get(Report::reportId);
        $deleted = $this->report->forceDeleteReport([Report::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        return ResponseHelper::delete();
    }
    public function allReport(Request $request){
        return ResponseHelper::select($this->report->allReportQuestion());
    }
}
