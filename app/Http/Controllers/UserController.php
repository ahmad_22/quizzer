<?php

namespace App\Http\Controllers;

use App\classes\ResponseHelper;
use App\Http\Requests\User\loginRequest;
use App\Http\Requests\User\signUpRequest;
use App\Http\Requests\User\sinupRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $user=null;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(loginRequest $request)
    {
        $data=$request->only(User::username,User::password);
        $user=$this->user->login($data);
        if(empty($user))
            return ResponseHelper::isEmpty();
        return ResponseHelper::select($user);
    }

    public function signUp(signUpRequest $request)
    {
        $data=$request->only($this->user->getFillable());
        $data['type']='student';
        unset($data[User::password]);
        $data['password']=Hash::make($request->get(User::password));

        $create=$this->user->createUser($data);
        if(empty($create))
            return ResponseHelper::isEmpty();
        $loginData=$request->only(User::username,User::password);
        $loginUser=$this->user->login($loginData);
        return ResponseHelper::select($loginUser);
    }

    public function logout(Request $request)
    {
        if (Auth::check())
        {
            $this->user->logout();
        }
        return ResponseHelper::select('logout');
    }
}
