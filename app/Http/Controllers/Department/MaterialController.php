<?php

namespace App\Http\Controllers\Department;

use App\classes\ResponseHelper;
use App\Http\Requests\Department\Material\createMaterialRequest;
use App\Http\Requests\Department\Material\deteleMaterialRequest;
use App\Http\Requests\Department\Material\getMaterialRequest;
use App\Http\Requests\Department\Material\updateMaterialRequest;
use App\Model\Department\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class MaterialController extends Controller
{
    private $material=null;

    /**
     * MaterialController constructor.
     * @param Material $material
     */
    public function __construct(Material $material)
    {
        $this->material = $material;
    }

    public function createMaterial(createMaterialRequest $request){
        $data=$request->only($this->material->getFillable());
        $created=$this->material->createMaterial($data);
        if(empty($created))
            return ResponseHelper::generalError();
        return ResponseHelper::insert($created);
    }

    public function updateMaterial(updateMaterialRequest $request){
        $id=$request->get(Material::materialId);
        $data=$request->only(Material::name);
        $updated=$this->material->updateMaterial([Material::id=>$id],$data);
        if(empty($updated))
            return ResponseHelper::generalError();
        return ResponseHelper::update($updated);
    }

    public function deleteMaterial(deteleMaterialRequest $request){
        $id=$request->get(Material::materialId);
        $deleted = $this->material->deleteMaterial([Material::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        return ResponseHelper::delete();
    }
    public function allMaterialDepartment(getMaterialRequest $request){
        if($request->has(Material::departmentId)){
            $departmentId=$request->get(Material::departmentId);
        }else
            $departmentId=$request->user()->department_id;
        return ResponseHelper::select($this->material->get([Material::departmentId=>$departmentId]));
    }
}
