<?php

namespace App\Http\Controllers\Department;

use App\classes\ResponseHelper;
use App\Http\Requests\Department\Department\createDepartmentRequest;
use App\Http\Requests\Department\Department\deteleDepartmentRequest;
use App\Http\Requests\Department\Department\updateDepartmentRequest;
use App\Model\Department\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    private $department=null;

    /**
     * DepartmentController constructor.
     * @param Department $department
     */
    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function createDepartment(createDepartmentRequest $request){
        $data=$request->only($this->department->getFillable());
        $created=$this->department->createDepartment($data);
        if(empty($created))
            return ResponseHelper::generalError();
        return ResponseHelper::insert($created);
    }

    public function updateDepartment(updateDepartmentRequest $request){
        $id=$request->get(Department::departmentId);
        $data=$request->only(Department::name);
        $updated=$this->department->updateDepartment([Department::id=>$id],$data);
        if(empty($updated))
            return ResponseHelper::generalError();
        return ResponseHelper::update($updated);
    }

    public function deleteDepartment(deteleDepartmentRequest $request){
        $id=$request->get(Department::departmentId);
        $deleted = $this->department->deleteDepartment([Department::id => $id]);
        if(empty($deleted))
            return ResponseHelper::generalError();
        return ResponseHelper::delete();
    }

    public function allDepartment(){
        return ResponseHelper::select($this->department->allDepartment());
    }

}
