<?php

namespace App\Model\Department;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Material extends Model
{
    use SoftDeletes;

    const table='materials';
    const materialId='material_id';
    const id='id';
    const name='name';
    const departmentId='department_id';
    const createdAt = 'created_at';
    const updatedAt = 'updated_at';
    const deletedAt = 'deleted_at';

    protected $table=self::table;
    protected $fillable=[self::name,self::departmentId];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::departmentId=>'integer'];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function createMaterial($data)
    {
        return self::create($data);
    }

    public function updateMaterial($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteMaterial($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteMaterial($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allDepartment()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
}
