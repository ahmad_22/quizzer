<?php

namespace App\Model\Department;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Department extends Model
{
    use SoftDeletes;

    const table='departments';
    const departmentId='department_id';
    const id='id';
    const name='name';
    const createdAt = 'created_at';
    const updatedAt = 'updated_at';
    const deletedAt = 'deleted_at';

    protected $table=self::table;
    protected $fillable=[self::name];
    protected $hidden=[self::updatedAt,self::deletedAt];

    public function material()
    {
        return $this->hasMany(Material::class);
    }

    public function createDepartment($data)
    {
        return self::create($data);
    }

    public function updateDepartment($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteDepartment($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteDepartment($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allDepartment()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
}
