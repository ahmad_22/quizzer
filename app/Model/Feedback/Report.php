<?php

namespace App\Model\Feedback;

use App\Model\Exam\Question;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Report extends Model
{
    use SoftDeletes;

    const table='reports';
    const reportId='report_id';
    const id='id';

    const questionId='question_id';
    const reportText='report_text';
    const isSee='is_see';
    const userId='user_id';
    const createdAt = 'created_at';
    const updatedAt = 'updated_at';
    const deletedAt = 'deleted_at';

    protected $table=self::table;
    protected $fillable=[self::questionId,self::userId,self::reportText,self::isSee];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::userId=>'integer',self::questionId=>'integer'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function createReport($data)
    {
        return self::create($data);
    }

    public function updateReport($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteReport($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteReport($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allReport()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }

    public function allReportQuestion()
    {
        return self::with(['question'=>function($question){
            $question->with('answer');
        }])->get();
    }
}
