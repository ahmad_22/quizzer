<?php

namespace App\Model\Feedback;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Feedback extends Model
{
    use SoftDeletes;

    const table='feedbacks';
    const feedbackId='feedback_id';
    const id='id';
    const feedbackText='feedback_text';
    const userId='user_id';
    const createdAt = 'created_at';
    const updatedAt = 'updated_at';
    const deletedAt = 'deleted_at';

    protected $table=self::table;
    protected $fillable=[self::feedbackText,self::userId];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::userId=>'integer'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createFeedback($data)
    {
        return self::create($data);
    }

    public function updateFeedback($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteFeedback($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteFeedback($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allFeedback()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
}
