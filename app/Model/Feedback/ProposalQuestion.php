<?php

namespace App\Model\Feedback;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class ProposalQuestion extends Model
{
    use SoftDeletes;

    const table='proposal_questions';
    const proposalQuestionId='proposal_question_id';
    const id='id';

    const materialId='material_id';
    const question='question';
    const answer1='answer1';
    const isTrue1='is_true1';
    const answer2='answer2';
    const isTrue2='is_true2';
    const answer3='answer3';
    const isTrue3='is_true3';
    const answer4='answer4';
    const isTrue4='is_true4';
    const userId='user_id';
    const createdAt = 'created_at';
    const updatedAt = 'updated_at';
    const deletedAt = 'deleted_at';

    protected $table=self::table;
    protected $fillable=[self::materialId,self::question,self::userId,self::answer1,self::answer2,self::answer3,self::answer4
    ,self::isTrue1,self::isTrue2,self::isTrue3,self::isTrue4];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::userId=>'integer'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createProposalQuestion($data)
    {
        return self::create($data);
    }

    public function updateProposalQuestion($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteProposalQuestion($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteProposalQuestion($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allProposalQuestion()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
    public function getProposalMaterial($materialId){
        return self::where(self::materialId,$materialId)->get();
    }
}
