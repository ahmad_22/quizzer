<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use phpDocumentor\Reflection\Types\Self_;

class ExamQuestion extends Model
{
    use SoftDeletes;

    const table='exam_questions';
    const ExamQuestionId='exam_question_id';

    const examId='exam_id';
    const questionId='question_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable=[self::examId,self::questionId];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::examId=>'integer',self::questionId=>'integer'];

    function question(){
        return $this->belongsTo(Question::class);
    }
    public function createExamQuestion($data)
    {
        return self::create($data);
    }
    public function insertExamQuestion($data)
    {
        return self::insert($data);
    }

    public function updateExamQuestion($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteExamQuestion($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteExamQuestion($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allExamQuestion()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
    public function getExamQuestion($examId,$questionIds){
       return Question::with('answer')
           ->join('exam_questions','exam_questions.question_id','=','questions.id')
           ->where('exam_questions.exam_id',$examId)
           ->whereIn('questions.id',$questionIds)
           ->select('questions.id','exam_questions.id as exam_questions_id','questions.question_text'
           ,'questions.material_id')
           ->get();
    }
}

