<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Answer extends Model
{
    use SoftDeletes;

    const table='answers';
    const answerId='answer_id';
    const id='id';

    const answerText='answer_text';
    const isTrue='is_true';
    const questionId='question_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable=[self::answerText,self::questionId,self::isTrue];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::questionId=>'integer'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function createAnswer($data)
    {
        return self::create($data);
    }

    public function insertAnswer($data)
    {
        return self::insert($data);
    }

    public function updateAnswer($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteAnswer($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteAnswer($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allAnswer()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
}
