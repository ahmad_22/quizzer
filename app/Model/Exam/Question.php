<?php

namespace App\Model\Exam;

use App\Model\Department\Material;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use phpDocumentor\Reflection\Types\Self_;

class Question extends Model
{
    use SoftDeletes;
    const table='questions';
    const id='id';
    const questionId='question_id';
    const questionText='question_text';
    const materialId='material_id';
    const userId='user_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable=[self::materialId,self::userId,self::questionText];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::materialId=>'integer',self::userId=>'integer'];

    function material(){
        return $this->belongsTo(Material::class);
    }
    function user(){
        return $this->belongsTo(User::class);
    }
    function answer(){
        return $this->hasMany(Answer::class,Answer::questionId);
    }

    public function createQuestion($data)
    {
        return self::create($data);
    }

    public function updateQuestion($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteQuestion($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteQuestion($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allQuestion()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }

    public function getRandomQuestion($number)
    {
        return self::inRandomOrder()->limit($number)->get();
    }
    public function getQuestionMaterial($materialId){
        return self::where(self::materialId,$materialId)->with('answer')->get();
    }
}
