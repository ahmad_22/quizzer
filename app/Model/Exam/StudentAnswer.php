<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class StudentAnswer extends Model
{
    use SoftDeletes;

    const table='student_answers';
    const studentAnswer='student_answers_id';

    const examQuestionId='exam_question_id';
    const studentAnswerId='student_answer_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable=[self::examQuestionId,self::studentAnswerId];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::examQuestionId=>'integer',self::studentAnswerId=>'integer'];

    public function createStudentAnswer($data)
    {
        return self::create($data);
    }
    public function insertStudentAnswer($data)
    {
        return self::insert($data);
    }

    public function updateStudentAnswer($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteStudentAnswer($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteStudentAnswer($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allStudentAnswer()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
}
