<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Exam extends Model
{
    use SoftDeletes;

    const table='exams';
    const examId='exam_id';
    const id='id';

    const name='name';
    const numberQuestion='number_question';
    const userId='user_id';
    const materialId='material_id';
    const createdAt ='created_at';
    const updatedAt ='updated_at';
    const deletedAt ='deleted_at';

    protected $table=self::table;
    protected $fillable=[self::name,self::userId,self::materialId];
    protected $hidden=[self::updatedAt,self::deletedAt];
    protected $casts=[self::userId=>'integer',self::materialId=>'integer'];

    function examQuestion(){
        return $this->hasMany(ExamQuestion::class,ExamQuestion::examId);
    }
    public function createExam($data)
    {
        return self::create($data);
    }

    public function updateExam($filter,$data)
    {
        return self::where($filter)
            ->update($data);
    }

    public function deleteExam($filter)
    {
        return self::where($filter)
            ->delete();
    }

    public function forceDeleteExam($filter){
        $delete=null;
        try {
            $delete=self::where($filter)->forceDelete();
        }catch (QueryException $exception){
            if ($exception->getCode()==23000)
                return 23;
        }
        return $delete;
    }

    public function find($filter)
    {
        return self::where($filter)
            ->first();
    }

    public function get($filter)
    {
        return self::where($filter)
            ->get();
    }

    public function allExam()
    {
        return self::all();
    }

    public function getFillable()
    {
        return $this->fillable;
    }
    public function getExam($examId){
        return Question::with('answer')
            ->join('exam_questions','exam_questions.question_id','=','questions.id')
            ->join('exams','exams.id','=','exam_questions.exam_id')
            ->join('student_answers','student_answers.exam_question_id','=','exam_questions.id')
            ->where('exams.id',$examId)
            ->select('questions.id','exam_questions.id as exam_questions_id','questions.question_text'
                ,'questions.material_id','exams.name as exam_name','student_answers.student_answer_id')
            ->get();
    }
}
